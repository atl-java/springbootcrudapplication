package az.atlacademy.SpringBootCRUDApp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tutorials_new")
public class Tutorial {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long tutorialId;

    @Column(nullable = false)
    private String title;

    private String description;
    private Boolean isPublished;
    private Boolean active = true;
}
