package az.atlacademy.SpringBootCRUDApp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class TutorialDto {
    private Long tutorialId;
    private String title;
    private String description;
    private Boolean published;
}
