package az.atlacademy.SpringBootCRUDApp.service;

import az.atlacademy.SpringBootCRUDApp.dto.TutorialDto;
import az.atlacademy.SpringBootCRUDApp.model.Tutorial;

import java.util.List;

public interface TutorialService {
    int addTutorial(Tutorial tutorial);
    int update(Long id, Tutorial tutorial);
    List<TutorialDto> findById(Long id);
    int deleteById(Long id);
    List<TutorialDto> findAll();
    int deleteAll();
}
