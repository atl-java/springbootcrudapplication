package az.atlacademy.SpringBootCRUDApp.service.impl;

import az.atlacademy.SpringBootCRUDApp.dto.TutorialDto;
import az.atlacademy.SpringBootCRUDApp.exception.dto.CustomerAlreadyExistsException;
import az.atlacademy.SpringBootCRUDApp.exception.dto.NoSuchCustomerExistsException;
import az.atlacademy.SpringBootCRUDApp.mapper.TutorialMapper;
import az.atlacademy.SpringBootCRUDApp.model.Tutorial;
import az.atlacademy.SpringBootCRUDApp.repository.TutorialRepository;
import az.atlacademy.SpringBootCRUDApp.service.TutorialService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class TutorialServiceImpl implements TutorialService {
    private final TutorialRepository tutorialRepository;
    private final TutorialMapper tutorialMapper;

    @Override
    public int addTutorial(Tutorial tutorial) {

        if (tutorialRepository.existsTutorialByTitle(tutorial.getTitle())) {
            throw new CustomerAlreadyExistsException("Customer already exist!");
        }

        tutorialRepository.save(tutorial);
        return 1;
    }

    @Override
    public int update(Long id, Tutorial tutorial) {
        List<Tutorial> activeTutorialList = tutorialRepository.findAllById(Arrays.asList(id))
                .stream()
                .filter(Tutorial::getActive)
                .collect(Collectors.toList());

        if (activeTutorialList.isEmpty()) {
            throw new NoSuchCustomerExistsException("No such customer to update with id: " + id);
        }

        for (Tutorial t : activeTutorialList) {
            t.setTitle(tutorial.getTitle());
            t.setDescription(tutorial.getDescription());
            t.setIsPublished(tutorial.getIsPublished());
        }

        tutorialRepository.saveAll(activeTutorialList);

        return 1;
    }

    @Override
    public List<TutorialDto> findById(Long id) {
        List<Tutorial> activeTutorialList = tutorialRepository.findAllById(Arrays.asList(id))
                .stream()
                .filter(Tutorial::getActive)
                .collect(Collectors.toList());

        if (activeTutorialList.isEmpty()) {
            throw new NoSuchCustomerExistsException("No such customer with id: " + id);
        }

        return activeTutorialList.stream()
                .map(tutorialMapper::tutorialToDto)
                .collect(Collectors.toList());
    }

    @Override
    public int deleteById(Long id) {
        List<Tutorial> activeTutorialList = tutorialRepository.findAllById(Arrays.asList(id))
                .stream()
                .filter(Tutorial::getActive)
                .collect(Collectors.toList());

        if (activeTutorialList.isEmpty()) {
            throw new NoSuchCustomerExistsException("There is no customer to delete with id: " + id);
        }

        for (Tutorial t : activeTutorialList) {
            t.setActive(false);
        }

        tutorialRepository.saveAll(activeTutorialList);

        return 1;
    }

    @Override
    public List<TutorialDto> findAll() {
        List<Tutorial> allActiveTutorialList = tutorialRepository.findAll()
                .stream()
                .filter(Tutorial::getActive)
                .collect(Collectors.toList());

        if (allActiveTutorialList.isEmpty()) {
            throw new NoSuchCustomerExistsException("Table is empty. Nothing to show!!");
        }

        return allActiveTutorialList.stream()
                .map(tutorialMapper::tutorialToDto)
                .collect(Collectors.toList());
    }

    @Override
    public int deleteAll() {
        List<Tutorial> allActiveTutorialList = tutorialRepository.findAll()
                .stream()
                .filter(Tutorial::getActive)
                .collect(Collectors.toList());

        if (allActiveTutorialList.isEmpty()) {
            throw new NoSuchCustomerExistsException("Nothing left to delete!");
        }

        for (Tutorial t : allActiveTutorialList) {
            t.setActive(false);
        }

        tutorialRepository.saveAll(allActiveTutorialList);

        return 1;
    }
}
