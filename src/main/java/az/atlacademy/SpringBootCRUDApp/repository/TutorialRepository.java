package az.atlacademy.SpringBootCRUDApp.repository;

import az.atlacademy.SpringBootCRUDApp.model.Tutorial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TutorialRepository extends JpaRepository<Tutorial, Long> {
    boolean existsTutorialByTitle(String title);
}
