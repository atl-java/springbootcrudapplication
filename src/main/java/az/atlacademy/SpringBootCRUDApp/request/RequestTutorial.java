package az.atlacademy.SpringBootCRUDApp.request;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RequestTutorial {

    @NotNull
    private Long tutorialId;

    @Size(min = 3, max = 15)
    private String title;

    @Size(min = 1, max = 100)
    private String description;

    @AssertTrue
    private Boolean isPublished;
}
