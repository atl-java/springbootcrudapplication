package az.atlacademy.SpringBootCRUDApp.mapper;

import az.atlacademy.SpringBootCRUDApp.dto.TutorialDto;
import az.atlacademy.SpringBootCRUDApp.model.Tutorial;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TutorialMapper {

    @Mapping(target = "published", source = "isPublished")
    TutorialDto tutorialToDto(Tutorial tutorial);

    @Mapping(target = "isPublished", source = "published")
    Tutorial dtoToTutorial(TutorialDto tutorialDto);
}
