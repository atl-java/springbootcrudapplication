package az.atlacademy.SpringBootCRUDApp.controller;

import az.atlacademy.SpringBootCRUDApp.dto.TutorialDto;
import az.atlacademy.SpringBootCRUDApp.model.Tutorial;
import az.atlacademy.SpringBootCRUDApp.service.TutorialService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/tutorials")
public class TutorialController {
    private final TutorialService tutorialService;

    @GetMapping
    public ResponseEntity<List<TutorialDto>> getAll(){
        return ResponseEntity.ok(tutorialService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<TutorialDto>> getById(@PathVariable Long id){
        return ResponseEntity.ok(tutorialService.findById(id));
    }

    @PostMapping
    public ResponseEntity addTutorial(@RequestBody Tutorial tutorial){
        return ResponseEntity.ok(tutorialService.addTutorial(tutorial));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity updateTutorial(@PathVariable Long id, @RequestBody Tutorial tutorial){
        return ResponseEntity.ok(tutorialService.update(id, tutorial));
    }

    @DeleteMapping
    public ResponseEntity deleteAll(){
        return ResponseEntity.ok(tutorialService.deleteAll());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Long id){
        return ResponseEntity.ok(tutorialService.deleteById(id));
    }
}
