package az.atlacademy.SpringBootCRUDApp.exception;

import az.atlacademy.SpringBootCRUDApp.exception.dto.CustomerAlreadyExistsException;
import az.atlacademy.SpringBootCRUDApp.exception.dto.NoSuchCustomerExistsException;
import az.atlacademy.SpringBootCRUDApp.exception.dto.NoSuchAnElementException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value= NoSuchCustomerExistsException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleException(NoSuchCustomerExistsException ex){
        return new ErrorResponse(
                HttpStatus.NOT_FOUND.value(), ex.getMessage());
    }

    @ExceptionHandler(value= CustomerAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorResponse handleException(CustomerAlreadyExistsException ex){
        return new ErrorResponse(
                HttpStatus.CONFLICT.value(), ex.getMessage());
    }

    @ExceptionHandler(value= NoSuchAnElementException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleException(NoSuchAnElementException ex){
        return new ErrorResponse(
                HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }
}
