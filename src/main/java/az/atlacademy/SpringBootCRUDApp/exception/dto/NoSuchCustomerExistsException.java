package az.atlacademy.SpringBootCRUDApp.exception.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class NoSuchCustomerExistsException extends RuntimeException{
    private String message;
}
